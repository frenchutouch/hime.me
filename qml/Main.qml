/*
 * Copyright (C) 2020  Romain Maneschi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * hide.me is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import Example 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'hide.me.frenchutouch'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        id: page
        anchors.fill: parent

        property var allServers: []
        property var selectedServer: []

        header: PageHeader {
            id: header
            title: i18n.tr('hide.me')

            trailingActionBar.actions: [
                Action {
                    text: i18n.tr("Free/Premium")
                    iconName: "preferences-desktop-login-items-symbolic"
                    onTriggered: {
                        if (serversUI.servers.length < page.allServers.length) {
                            serversUI.servers = refreshArray(page.allServers);
                        } else {
                            serversUI.servers = filterFree(page.allServers);
                        }
                    }
                }
            ]
        }

        Servers {
            id: serversUI
            anchors.top: header.bottom
            anchors.bottom: buttonsActions.visible ? buttonsActions.top : parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right

            onSelect: {
                if (selected) {
                    page.selectedServer.push(server.hostname);
                    page.selectedServer = refreshArray(page.selectedServer);
                } else {
                    page.selectedServer = page.selectedServer.filter(function(s) { 
                        return s != server.hostname 
                    });
                }
            }
        }

        Rectangle {
            id: buttonsActions
            visible: page.selectedServer.length > 0
            color: theme.palette.normal.background

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            height: units.gu(6)
            
            RowLayout {
                
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    margins: units.gu(2)
                }
                spacing: units.gu(2)

                Button {
                    text: i18n.tr("Clear")
                    onClicked: {
                        serversUI.unselectAll();
                        page.selectedServer = [];
                    }
                    Layout.fillWidth: true
                }

                Button {
                    text: i18n.tr("Add")
                    onClicked: Example.write(page.selectedServer)
                    Layout.fillWidth: true
                }
            }
        }

        Component.onCompleted: {
            request('https://api.hide.me/v1/external/passepartout', function (data) {
                console.log(data.length);
                page.allServers = data;
                serversUI.servers = filterFree(data);
            });
        }
    }

    function request(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if(xhr.readyState === XMLHttpRequest.DONE && xhr.status >= 200 && xhr.status < 300) {
                callback(JSON.parse(xhr.responseText));
            }
        };
        xhr.open('GET', url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send();
    }

    function filterFree(servers) {
        return servers.filter(function(s) { return s.tags[0] === "free"; });
    }

    function refreshArray(arr) {
        return arr.filter(function(s) { return true; });
    }
}
