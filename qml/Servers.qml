import QtQuick 2.7
import Ubuntu.Components 1.3
import QtGraphicalEffects 1.0

ListView {
    id: list

    property var servers: 0

    signal select(var server, bool selected)

    function unselectAll() {
        for(var child in list.contentItem.children) {
            list.contentItem.children[child].selected = false;
        }

    }

    clip: true
    model: servers
    delegate: ListItem {
        width: parent.width

        property var server: servers[index]
        property bool selected: false

        Row {
            height: parent.height
            width: parent.width
            padding: units.gu(2)
            spacing: units.gu(2)

            CheckBox {
                checked: selected
            }

            Image {
                height: units.gu(2)
                width: units.gu(2)
                source: "https://hide.me/images/flags/png/"+server.flag+".png"
                cache: true
            }

            Label { 
                text: server.displayName 
                width: parent.width
                elide: Text.ElideLeft
                wrapMode: Text.Wrap
                maximumLineCount: 1
                textSize: Label.Large
            }
        }

        onClicked: {
            selected = !selected
            select(server, selected)
        }
    }
}